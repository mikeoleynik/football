10.times {
  Match.create!(title: Faker::Games::HalfLife.location)
}

team_a = Team.create!(title: 'A')
team_b = Team.create!(title: 'B')

20.times {
  TeamMatch.create!(team: team_a, match: Match.find(rand(1..10)))
  TeamMatch.create!(team: team_b, match: Match.find(rand(1..10)))
}

11.times do
  Player.create!(name: Faker::Name.name, team: team_a)
  Player.create!(name: Faker::Name.name, team: team_b)
end

running1 = Indication.create!(title: 'Пробежал 10 км')
running2 = Indication.create!(title: 'Пробежал 20 км')
running3 = Indication.create!(title: 'Пробежал 30 км')
pass1 = Indication.create!(title: 'сделал 50+ % точных передач')
pass2 = Indication.create!(title: 'сделал 60+ % точных передач')
pass3 = Indication.create!(title: 'сделал 70+ % точных передач')
goal1 = Indication.create!(title: 'забил 3 гола')
goal2 = Indication.create!(title: 'забил 4 гола')
goal3 = Indication.create!(title: 'забил 5 голов')

10.times {
  PlayerParticipation.create!(player: Player.find(rand(1..22)),
                              match: Match.find(rand(1..10)),
                              indication: Indication.find(rand(1..9)))
}
