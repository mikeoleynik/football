class CreatePlayerParticipations < ActiveRecord::Migration[5.2]
  def change
    create_table :player_participations do |t|
      t.integer :player_id, index: true, null: false
      t.integer :match_id, index: true, null: false
      t.integer :indication_id, index: true, null: false

      t.timestamps
    end
  end
end
