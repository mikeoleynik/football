class CreatePlayers < ActiveRecord::Migration[5.2]
  def change
    create_table :players do |t|
      t.string :name
      t.integer :team_id, index: true, null: false

      t.timestamps
    end
  end
end
