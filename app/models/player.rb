class Player < ApplicationRecord
  belongs_to :team

  has_many :player_participations
  has_many :matches, through: :player_participations
  has_many :indications, through: :player_participations

  def self.best_players(indication)
    FindTopFive.new(initial_scope: self, indication: indication).call
  end

  def meet_target?(indication, match)
    player_participations.where(match: match).each do |pp|
      return true if pp.indication.title == indication
    end
    false
  end

  def meet_one_times?(indication)
    team.matches.includes(:indications).last(5).map do |match|
      return true if match.indications.pluck(:title).include?(indication)
    end
    false
  end
end
