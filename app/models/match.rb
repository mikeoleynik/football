class Match < ApplicationRecord
  has_many :player_participations
  has_many :indications, through: :player_participations

  has_many :team_matches
  has_many :teams, through: :team_matches
end
