class Indication < ApplicationRecord
  has_many :player_participations
  has_many :matches, through: :player_participations
  has_many :players, through: :player_participations
end
