class PlayerParticipation < ApplicationRecord
  belongs_to :match
  belongs_to :indication
  belongs_to :player
end
