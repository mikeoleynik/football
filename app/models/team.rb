class Team < ApplicationRecord
  has_many :players

  has_many :team_matches
  has_many :matches, through: :team_matches

  def top_five(indication)
    FindTopFive.new(initial_scope: players, indication: indication).call
  end
end
