class FindTopFive
  attr_reader :initial_scope, :indication

  def initialize(params)
    @initial_scope = params[:initial_scope]
    @indication = params[:indication]
  end

  def call
    initial_scope
    .joins(:indications)
    .where('indications.title = ?', indication)
    .group(:name)
    .order('count_indications_title DESC')
    .limit(5)
    .count("indications.title")
  end
end
