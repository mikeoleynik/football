require 'rails_helper'

describe 'Find Top Five', type: :request do
  include_context "shared participation players"

  it 'among all players' do
    find_top_five(Player.all, "сделал 50+ % точных передач")
    expect(@players.count).to eq 3
  end

  it 'among players in team' do
    find_top_five(team_a.players, "сделал 50+ % точных передач")
    expect(@players.count).to eq 2
  end
end
