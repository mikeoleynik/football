require 'rails_helper'

RSpec.describe Player, type: :model do
  include_context "shared participation players"

  it 'make indicator in match' do
    expect(player1.meet_target?(pass.title, match.id)).to eq true
  end

  it 'completed specific indicator at least 1 time in previous 5 matches of team' do
    expect(player1.meet_one_times?(pass.title)).to eq true
  end
end
