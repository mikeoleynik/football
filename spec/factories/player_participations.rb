FactoryBot.define do
  factory :player_participation do
    player_id { nil }
    match_id { nil }
    indication_id { nil }
  end
end
