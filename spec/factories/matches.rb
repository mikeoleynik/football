FactoryBot.define do
  factory :match do
    title { Faker::Games::HalfLife.location }
  end
end
