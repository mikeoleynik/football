FactoryBot.define do
  factory :team_a, class: 'Team' do
    title { 'A' }
  end

  factory :team_b, class: 'Team' do
    title { 'B' }
  end
end