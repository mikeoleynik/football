FactoryBot.define do
  factory :player do
    name { Faker::Name.name }
    team_id { nil }
  end
end