RSpec.shared_context "shared participation players" do
  let(:match) { create(:match) }
  let(:team_a) { create(:team_a) }
  let(:team_b) { create(:team_b) }
  let!(:team_match) { create(:team_match, team: team_a, match: match) }
  let!(:team_match2) { create(:team_match, team: team_b, match: match) }
  let!(:player1) { create(:player, team: team_a) }
  let!(:player2) { create(:player, team: team_a) }
  let!(:player3) { create(:player, team: team_b) }
  let!(:run) { create(:indication, title: 'Пробежал 10 км') }
  let!(:pass) { create(:indication, title: 'сделал 50+ % точных передач') }
  let!(:goal) { create(:indication, title: 'забил 3 гола') }
  let!(:player_participation1) { create(:player_participation, player: player1,
                                                              match: match,
                                                              indication: pass) }
  let!(:player_participation2) { create(:player_participation, player: player2,
                                                              match: match,
                                                              indication: pass) }
  let!(:player_participation3) { create(:player_participation, player: player3,
                                                              match: match,
                                                              indication: pass) }
end